const Promise = require("bluebird");
const Operation = require("../operation/index.es6");

// Small interface [example] to keep the overall system out of your code.
class Interface {

  constructor({ task, block }) {

    this.cardTask = task;
    this.blockMetadata = block;

  }

  creator() {

    let cardBlockPromise = new Promise((resolve, reject) => {

      // this.task is the card creation task object
      // this task . widget is the jQuery-ui widget being created
      // this.task.widget.element is the html element that belongs to the widget

      let { element } = this.cardTask.widget;
      let { cardTask, blockMetadata } = this;
      let operation = new Operation({ cardTask });

      // NOTE: resolve, reject, element are not positional, nor in any way restrictive here
      // you can make up new parameters as you go.
      // the this.task (the card creation task)  will bring in new information over the years
      // the reason why this.task.widget is not merely this.widget is for new metadata and upgrades
      // expect new API information here this.task.someNewFantasticPieceOfInformation that is really useful.

      operation.connection({

        // Promise
        resolve,
        reject,

        // Helpful Information Isolated for the specific task at hand
        element, // print to this
        block: blockMetadata, // use information from here

        // there are many ways to attach destructor payload...
        // were we pass it into the Operation for potential assignment
        destructor: this.destructor

      });

    }); // end creation of cardBlockPromise
    return cardBlockPromise;
  }

  destructor() {}
}

module.exports = Interface;
