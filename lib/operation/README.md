// main block resolution - if multiple operations are required - call this after they are all complete (ex. via promise.each);

/*

Example of many serial operations at this point

var Promise = require("bluebird");
... someArrayOfItems

let convertItemToPromise = (someItem) => {
  let promisifiedItem = new Promise((resolve, reject) => {
    // do the thing
    resolve();
  });
  return promisifiedItem;
});

Promise.each(someArrayOfItems, convertItemToPromise).done(() => {
  // console.log("BLOCKS FINISHED");
  resolve();
});

*/
