const Promise = require("bluebird");


class Operation {
  constructor({ cardTask }) {
    this.cardTask = cardTask;
  }

  // MAIN connection with Interface
  // control incoming information from the interface
  connection( { resolve, reject, element, block } ){

    require.ensure(["h13a-docs"], function(require) {
      const docs = require("h13a-docs");

    // BLOCK OPTIONS
    // extract configuration information from the block object here
    let {locations} = block;

    // REACT TO OPTIONS
    // keep things flat please
    locations && locations.forEach(location => {
        // console.log('FILE BLOCK READ LOCATION:', location);
        let content = docs.read(location);
        let $content = $(` <div class="card-component file-block"> ${content} </div> `);
        element.append( $content );
    });

    // WHEN FINISHED RESOLVE BLOCK OPERATION
    resolve();


    // ...
    });
  } // end connection

} // close Operation


module.exports = Operation;
