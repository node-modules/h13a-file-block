
// This is the interface between card block needs and block operation.
// It is meant to lower the noise in the actual Operation
const Interface = require("./lib/interface/index.es6");

module.exports = Interface;
